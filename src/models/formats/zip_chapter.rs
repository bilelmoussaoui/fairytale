use super::utils;
use super::Error;
use crate::models::chapter::Paginable;
use std::fs::File;
use std::io::Read;

pub struct ZipChapter {
    archive: zip::read::ZipArchive<std::fs::File>,
    current_page: usize,
    pages: Vec<String>, // the file name
}

impl ZipChapter {
    pub fn new(chapter_path: String) -> Result<Self, Error> {
        let file = File::open(&chapter_path)?;
        let mut archive = zip::ZipArchive::new(file)?;
        let mut pages: Vec<String> = Vec::new();
        for i in 0..archive.len() {
            let file = archive.by_index(i).unwrap();
            let filename = file.name();
            if utils::is_image(filename) {
                pages.push(filename.to_string());
            }
        }
        pages.sort_by(|a, b| a.cmp(&b));

        let chapter = ZipChapter {
            archive: archive,
            pages,
            current_page: 0,
        };
        Ok(chapter)
    }
}

impl Paginable for ZipChapter {
    fn get_page(&mut self, page_nr: usize) -> Result<glib::Bytes, Error> {
        match &self.pages.get(page_nr) {
            Some(filename) => {
                let mut file = self.archive.by_name(&filename)?;

                let mut bytes: Vec<u8> = Vec::new();
                file.read_to_end(&mut bytes)?;

                Ok(glib::Bytes::from(&bytes))
            }
            None => bail!("Page not found"),
        }
    }

    fn get_current_page(&self) -> usize {
        self.current_page
    }
    fn total_pages(&self) -> usize {
        self.pages.len() as usize
    }
}
