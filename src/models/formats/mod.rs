mod dir_chapter;
mod rar_chapter;
mod utils;
mod zip_chapter;

pub use crate::models::Error;
pub use dir_chapter::DirChapter;
pub use rar_chapter::RarChapter;
pub use zip_chapter::ZipChapter;
