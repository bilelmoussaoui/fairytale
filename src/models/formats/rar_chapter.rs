use super::Error;
use crate::models::chapter::Paginable;

pub struct RarChapter {
    archive: unrar::archive::OpenArchive,
    current_page: usize,
    pages: Vec<String>, // the file name
}

impl RarChapter {
    pub fn new(chapter_path: String) -> Result<Self, Error> {
        let archive = unrar::archive::Archive::new(chapter_path);
        let mut rar = archive.open(unrar::archive::OpenMode::List, None, unrar::archive::Operation::Skip).unwrap(); // don't hate me for this
        let archive_content = rar.process().unwrap();

        let mut pages: Vec<String> = Vec::new();
        for i in 0..archive_content.len() {
            let file = archive_content.get(i).unwrap();
            /*if utils::is_image(filename) {
                pages.push(filename.to_string());
            }*/
        }
        pages.sort_by(|a, b| a.cmp(&b));

        let chapter = RarChapter { archive: rar, pages, current_page: 0 };
        Ok(chapter)
    }
}

impl Paginable for RarChapter {
    fn get_page(&mut self, page_nr: usize) -> Result<glib::Bytes, Error> {
        match &self.pages.get(page_nr) {
            Some(filename) => {
                //let mut file = self.archive.get(&filename)?;

                let bytes: Vec<u8> = Vec::new();
                //file.read_to_end(&mut bytes)?;

                Ok(glib::Bytes::from(&bytes))
            }
            None => bail!("Page not found"),
        }
    }

    fn get_current_page(&self) -> usize {
        self.current_page
    }
    fn total_pages(&self) -> usize {
        self.pages.len() as usize
    }
}
