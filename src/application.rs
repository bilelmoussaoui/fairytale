use crate::config;
use crate::models::{Chapter, Viewable};
use crate::widgets::{View, Window};
use gio::prelude::*;
use gtk::prelude::*;
use gtk::SettingsExt;
use std::env;
use std::{cell::RefCell, rc::Rc};

use glib::{Receiver, Sender};
pub enum Action {
    OpenFile,
    ViewReader,
    ViewLibrary,
    LoadURI(String),
    LoadChapter(Chapter),
    LoadViewable(Rc<Box<Viewable>>),
}

pub struct Application {
    app: gtk::Application,
    window: Rc<RefCell<Window>>,
    sender: Sender<Action>,
    receiver: RefCell<Option<Receiver<Action>>>,
}

impl Application {
    pub fn new() -> Rc<Self> {
        let app = gtk::Application::new(Some(config::APP_ID), Default::default()).unwrap();

        glib::set_application_name(&format!("{}Fairy Tale", config::NAME_PREFIX));
        glib::set_prgname(Some("fairytale"));

        let (sender, r) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let receiver = RefCell::new(Some(r));

        let window = Window::new(sender.clone());

        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/FairyTale/shortcuts.ui");
        let dialog: gtk::ShortcutsWindow = builder.get_object("shortcuts").unwrap();
        window.borrow().widget.set_help_overlay(Some(&dialog));

        let application = Rc::new(Self { app, window, sender, receiver });

        application.setup_gactions();
        application.setup_signals();
        application.setup_css();
        application
    }

    pub fn run(&self, app: Rc<Self>) {
        info!("{}Fairy Tale ({})", config::NAME_PREFIX, config::APP_ID);
        info!("Version: {} ({})", config::VERSION, config::PROFILE);
        info!("Datadir: {}", config::PKGDATADIR);

        let app = app.clone();
        let receiver = self.receiver.borrow_mut().take().unwrap();
        receiver.attach(None, move |action| app.do_action(action));

        let args: Vec<String> = env::args().collect();
        self.app.run(&args);
    }

    fn setup_gactions(&self) {
        // Quit
        let app = self.app.clone();
        let simple_action = gio::SimpleAction::new("quit", None);
        simple_action.connect_activate(move |_, _| app.quit());
        self.app.add_action(&simple_action);
        self.app.set_accels_for_action("app.quit", &["<primary>q"]);

        // About
        let window = self.window.borrow().widget.clone();
        let simple_action = gio::SimpleAction::new("about", None);
        simple_action.connect_activate(move |_, _| {
            let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/FairyTale/about_dialog.ui");
            let about_dialog: gtk::AboutDialog = builder.get_object("about_dialog").unwrap();
            about_dialog.set_transient_for(Some(&window));

            about_dialog.connect_response(|dialog, _| dialog.destroy());
            about_dialog.show();
        });
        self.app.add_action(&simple_action);

        self.app.set_accels_for_action("reader.page('next')", &["Right"]);
        self.app.set_accels_for_action("reader.page('previous')", &["Left"]);
        self.app.set_accels_for_action("window.library", &["Escape"]);
        self.app.set_accels_for_action("reader.zoom-in", &["<primary>plus"]);
        self.app.set_accels_for_action("reader.zoom-out", &["<primary>minus"]);
        self.app.set_accels_for_action("window.open", &["<primary>O"]);
        self.app.set_accels_for_action("win.show-help-overlay", &["<primary>question"]);
    }

    fn setup_signals(&self) {
        let window = self.window.borrow().widget.clone();
        self.app.connect_activate(move |app| {
            let gtk_settings = gtk::Settings::get_default().unwrap();
            gtk_settings.set_property_gtk_application_prefer_dark_theme(true);

            window.set_application(Some(app));
            app.add_window(&window);
            window.present();
        });
    }

    fn setup_css(&self) {
        let theme = gtk::IconTheme::get_default().unwrap();
        theme.add_resource_path("/com/belmoussaoui/FairyTale/icons");

        let p = gtk::CssProvider::new();
        gtk::CssProvider::load_from_resource(&p, "/com/belmoussaoui/FairyTale/style.css");
        gtk::StyleContext::add_provider_for_screen(&gdk::Screen::get_default().unwrap(), &p, 500);
    }

    fn open_file(&self) {
        let open_dialog = gtk::FileChooserNative::new(Some("Open File"), Some(&self.window.borrow().widget), gtk::FileChooserAction::Open, Some("Open"), Some("Cancel"));

        let zip_filter = gtk::FileFilter::new();
        zip_filter.set_name(Some("application/zip"));
        zip_filter.add_mime_type("application/zip");

        open_dialog.add_filter(&zip_filter);

        if gtk::ResponseType::from(open_dialog.run()) == gtk::ResponseType::Accept {
            let uri = open_dialog.get_uri().unwrap().to_string();
            self.sender.send(Action::LoadURI(uri)).unwrap();
        };
        open_dialog.destroy();
    }

    fn do_action(&self, action: Action) -> glib::Continue {
        match action {
            Action::OpenFile => self.open_file(),
            Action::LoadURI(uri) => {
                let file = gio::File::new_for_uri(&uri);
                let path = file.get_path().unwrap();
                self.window.borrow().add_to_library(path.to_str().unwrap());
            }
            Action::ViewReader => self.window.borrow().set_view(View::Reader),
            Action::ViewLibrary => self.window.borrow().set_view(View::Library),
            Action::LoadChapter(chapter) => self.window.borrow().read_chapter(chapter),
            Action::LoadViewable(viewable) => self.window.borrow().load(viewable),
        };

        glib::Continue(true)
    }
}
