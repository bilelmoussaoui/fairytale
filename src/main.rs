extern crate pretty_env_logger;
#[macro_use]
extern crate log;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate glib;
#[macro_use]
extern crate serde_derive;

use gettextrs::*;
mod api;
mod application;
mod config;
mod models;
mod schema;

mod widgets;
mod window_state;

use application::Application;
use config::{LOCALEDIR, PACKAGE_NAME};

fn main() {
    pretty_env_logger::init();
    gtk::init().unwrap_or_else(|_| panic!("Failed to initialize GTK."));

    // Prepare i18n
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(PACKAGE_NAME, LOCALEDIR);
    textdomain(PACKAGE_NAME);

    let res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/" + &config::APP_ID.to_owned() + ".gresource").expect("Could not load resources");
    gio::resources_register(&res);

    let app = Application::new();
    app.run(app.clone());
}
