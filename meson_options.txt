option (
  'profile',
  type: 'combo',
  choices: [
    'default',
    'development'
  ],
  value: 'default',
  description: 'The build profile for Fairy Tale. One of "default" or "development".'
)
