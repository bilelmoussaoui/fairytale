use gdk::prelude::*;
use gtk::prelude::*;
use std::cell::RefCell;
use std::rc::Rc;

pub struct CoverImage {
    pub widget: gtk::Box,
    pixbuf: Rc<RefCell<Option<gdk_pixbuf::Pixbuf>>>,
}

impl CoverImage {
    pub fn new(pixbuf: Option<gdk_pixbuf::Pixbuf>) -> Rc<Self> {
        let widget = gtk::Box::new(gtk::Orientation::Horizontal, 0);

        let cover_image = Rc::new(Self {
            widget,
            pixbuf: Rc::new(RefCell::new(pixbuf)),
        });

        cover_image.init(cover_image.clone());
        cover_image
    }

    fn init(&self, s: Rc<Self>) {
        let image = gtk::DrawingArea::new();
        image.set_hexpand(true);
        image.set_vexpand(true);
        image.set_halign(gtk::Align::Fill);
        image.set_valign(gtk::Align::Fill);
        self.widget.set_size_request(120, 250);
        self.widget.pack_start(&image, true, true, 0);

        let cover_image = s.clone();
        image.connect_draw(move |widget, ctx| {
            let allocation = widget.get_allocation();
            match cover_image.pixbuf.clone().borrow().clone() {
                Some(pixbuf) => {
                    let pixbuf = pixbuf.scale_simple(allocation.width, allocation.height, gdk_pixbuf::InterpType::Bilinear).unwrap();
                    ctx.set_source_pixbuf(&pixbuf, 0.0, 0.0);
                    ctx.paint();
                }
                None => (),
            }
            gtk::Inhibit(false)
        });
        let drawing_area = image.clone();
        self.widget.get_toplevel().unwrap().connect_size_allocate(move |widget, allocation| {
            println!("allocation {:#?}", allocation);
            if allocation.width > 400 {
                drawing_area.set_size_request(250, 400);
            } else if allocation.width > 250 {
                drawing_area.set_size_request(200, 350);
            } else if allocation.width > 200 {
                drawing_area.set_size_request(120, 250);
            }
        });

        image.show();
        self.widget.show();
    }
}
