use crate::models::database;
use crate::schema::chapters;
use diesel::RunQueryDsl;
pub use failure::Error;
use gio::prelude::*;
use std::fs;
use std::path::{Path, PathBuf};
extern crate nanoid;

use diesel::prelude::*;

#[derive(Queryable, PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct Chapter {
    pub id: i32,
    pub title: String,
    pub path: String,
    pub cover_path: String,
    pub created_date: String,
}

impl Chapter {
    pub fn get_created_at(&self) -> String {
        self.created_date.clone()
    }
}

impl Viewable for Chapter {
    fn get_title(&self) -> String {
        self.title.clone()
    }

    fn get_cover_image(&self) -> Result<PathBuf, Error> {
        Ok(Path::new(&self.cover_path).to_path_buf())
    }
}

pub trait Paginable {
    fn get_page(&mut self, nr_page: usize) -> Result<glib::Bytes, Error>;
    fn get_current_page(&self) -> usize;

    fn get_cover(&mut self) -> Result<PathBuf, Error> {
        let mut cache_file: PathBuf = [glib::get_user_cache_dir().unwrap().to_str().unwrap(), "fairytale", "covers"].iter().collect();
        fs::create_dir_all(&cache_file)?;
        cache_file.push(nanoid::simple().as_str());

        let bytes = self.get_page(0)?;
        let file = gio::File::new_for_path(&cache_file);
        let ostream = file.replace(None, false, gio::FileCreateFlags::REPLACE_DESTINATION, gio::NONE_CANCELLABLE)?;
        ostream.write_bytes(&bytes, gio::NONE_CANCELLABLE)?;
        ostream.close(gio::NONE_CANCELLABLE)?;

        Ok(cache_file)
    }

    fn next(&mut self) -> Result<gio::MemoryInputStream, Error> {
        let page_nr = self.get_current_page() + 1;
        self.get_stream(page_nr)
    }
    fn previous(&mut self) -> Result<gio::MemoryInputStream, Error> {
        let page_nr = self.get_current_page() - 1;
        self.get_stream(page_nr)
    }
    fn first(&mut self) -> Result<gio::MemoryInputStream, Error> {
        self.get_stream(0)
    }
    fn total_pages(&self) -> usize;

    fn get_stream(&mut self, page_nr: usize) -> Result<gio::MemoryInputStream, Error> {
        let bytes = self.get_page(page_nr)?;
        let stream = gio::MemoryInputStream::new_from_bytes(&bytes);
        Ok(stream)
    }
}

#[derive(Insertable)]
#[table_name = "chapters"]
pub struct NewChapter {
    pub title: String,
    pub path: String,
    pub cover_path: String,
}

impl database::Insert<Chapter> for NewChapter {
    type Error = database::Error;
    fn insert(&self) -> Result<Chapter, database::Error> {
        let db = database::connection();
        let conn = db.get()?;

        info!("Inserting {:?}", self.path);
        diesel::insert_into(chapters::table).values(self).execute(&conn)?;

        chapters::table.order(chapters::columns::id.desc()).first::<Chapter>(&conn).map_err(From::from)
    }
}

pub trait Viewable {
    fn get_title(&self) -> String;
    fn get_cover_image(&self) -> Result<PathBuf, Error>;
}
